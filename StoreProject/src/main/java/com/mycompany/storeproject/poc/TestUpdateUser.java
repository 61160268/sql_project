/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class TestUpdateUser {
        public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "UPDATE user SET name = ?,tel = ?,password = ? WHERE  id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"mark 1");
            stmt.setString(2,"0951253214");
            stmt.setString(3,"password");
            stmt.setInt(4,5);
            int row = stmt.executeUpdate();
           
            System.out.println("Affect row "+ row );
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }

}
